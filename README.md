# Resume

This repository hosts my resume as a LaTeX source file, along with a gitlab
pipeline that will build and deploy my resume to my personal website that I have
running in the cloud.

## [Website Repository](https://gitlab.com/trevdev1/personal-website)
